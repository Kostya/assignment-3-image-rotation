#ifndef IMAGE_TRANSFORMER_TRANSFORM_FORMAT_H
#define IMAGE_TRANSFORMER_TRANSFORM_FORMAT_H
#include "inner_format.h"
enum angle {
    ZERO = 0,
    QUARTER = 90,
    HALF = 180,
    MINUS_QUARTER = 270
};
struct maybe_image rotate(struct image const * source, enum angle angle);
#endif //IMAGE_TRANSFORMER_TRANSFORM_FORMAT_H
