#include "inner_format.h"
#include <stdint.h>
#include <stdlib.h>

struct maybe_image create_maybe_image(void){
    return (struct maybe_image) {0};
}

struct maybe_image initialize_image(uint64_t w, uint64_t h) {
    struct maybe_image new_maybe_image = create_maybe_image();
    struct pixel* new_data = malloc(sizeof(struct pixel) * w * h);
    if (new_data == NULL) {
        return new_maybe_image;
    }
    struct image new_image = {.width = w, .height = h, .data = new_data};
    new_maybe_image.image = new_image;
    new_maybe_image.valid = true;
    return new_maybe_image;
}

void deinitialize_image(struct image* img) {
    img->width = 0;
    img->height = 0;
    free(img->data);
    img = NULL;
}
