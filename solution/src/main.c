#include "inner_format.h"
#include "input_format.h"
#include "transform_format.h"
#include <stdio.h>
#include <stdlib.h>

/* return value 4 - close error
 * return value 5 - close error */
int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning
    if (argc != 4) {
        fprintf(stderr, "wrong count variables");
        return -1;
    }
    FILE *file = fopen(argv[1], "rb");
    if (file == NULL) {
        fprintf(stderr, "File is not exist");
        return READ_WRONG_PATH;
    }
    struct image image;
    switch (from_bmp(file, &image)) {
        case READ_OK:
            break;
        case SYSTEM_ERROR:
            fprintf(stderr, "memory allocation error");
            return SYSTEM_ERROR;
        default:
            fprintf(stderr, "Bad picture");
            deinitialize_image(&image);
            fclose(file);
            return READ_INVALID_SIGNATURE;
    }
    if (fclose(file) != 0) {
        fprintf(stderr, "Close error");
        return 4;
    }
    struct maybe_image rotate_maybe_image;
    switch (strtol(argv[3], NULL, 10)) {
        case 0 :
            rotate_maybe_image = rotate(&image, ZERO);
            break;
        case 90:
        case -270:
            rotate_maybe_image = rotate(&image, QUARTER);
            break;
        case 180:
        case -180:
            rotate_maybe_image = rotate(&image, HALF);
            break;
        case 270:
        case -90:
            rotate_maybe_image = rotate(&image, MINUS_QUARTER);
            break;
        default:
            fprintf(stderr, "Wrong angle");
            deinitialize_image(&image);
            return -1;
    }
    struct image rotate_image;
    if (!rotate_maybe_image.valid) {
        fprintf(stderr, "memory allocation error");
        return SYSTEM_ERROR;
    } else {
        rotate_image = rotate_maybe_image.image;
    }

    file = fopen(argv[2], "wb");
    if (file == NULL) {
        fprintf(stderr, "Wrong path for target file");
        deinitialize_image(&image);
        deinitialize_image(&rotate_image);
        return -1;
    }
    switch (to_bmp(file, &rotate_image)) {
        case WRITE_OK:
            fprintf(stdout, "All is OK");
            deinitialize_image(&image);
            deinitialize_image(&rotate_image);
            if (fclose(file) != 0) {
                fprintf(stderr, "Close error");
                return 4;
            }
            return 0;
        case WRITE_ERROR:
            fprintf(stderr, "Wrong file");
            deinitialize_image(&image);
            deinitialize_image(&rotate_image);
            if (fclose(file) != 0) {
                fprintf(stderr, "Close error");
            }
            return WRITE_ERROR;
    }
}
